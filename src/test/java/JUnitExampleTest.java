// this file is just a simple JUnit test(s)

import transactionservice.Entities.Transaction;
import transactionservice.Logic.TransactionController;
import transactionservice.Logic.TransactionManager;
import gherkin.deps.com.google.gson.Gson;
import messaging.EventSender;
import org.junit.*;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.*;

/**
 * @author Sebastian Hoppe - s154306
 */
public class JUnitExampleTest {
    TransactionController transactionController;
    TransactionManager tm;

    @Before
    public void setUp() {
        // code executed before each test
        transactionController = new TransactionController(mock(EventSender.class));
        tm = mock(TransactionManager.class);
        transactionController.tm = tm;
    }

    @Test
    public void testCreateReportMethodSuccess() throws Exception {
        // Data
        String merchantId = "merch1";
        String customerId = "cust1";
        String token = "token1";
        String desc = "desc1";
        LocalDate period = LocalDate.now().minusMonths(1);
        boolean isMerchant = true;
        Object[] userType = {"", "merchant" };

        //FillDB
        Transaction transaction = new Transaction(new BigDecimal(100), merchantId, customerId, token, desc);
        List<Transaction> transactionList = new ArrayList<>();
        transactionList.add(transaction);

        transactionController.transactionDAO.addTransaction(transaction);

        doReturn(userType).when(tm).getUserType(merchantId);
        transactionList.get(0).setCustomerId("");

        String expected = new Gson().toJson(transactionList);
        String actual = transactionController.getReport(merchantId, period);

        assertEquals(expected, actual);
    }

    @Test
    public void testCreateTransaction() {
        // Data
        BigDecimal amount = new BigDecimal(100);
        String merchantId = "merc2";
        String customerId = "cust2";
        String token = "token2";
        String desc = "desc2";

        Transaction expected = new Transaction(amount, merchantId, customerId, token, desc);
        expected.setId("3");

        transactionController.createTransaction(amount, merchantId, customerId, token, desc);

        Transaction actual = transactionController.transactionDAO.getByToken(token);

        assertEquals(new Gson().toJson(expected), new Gson().toJson(actual));
    }

}
