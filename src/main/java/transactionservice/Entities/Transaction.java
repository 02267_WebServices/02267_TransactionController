package transactionservice.Entities;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * @author Sebastian Hoppe - s154306
 */
public class Transaction implements ITransaction {
    private String id;
    private BigDecimal amount;
    private String merchantId;
    private String customerId;
    private String token;
    private String description;
    private LocalDate timestamp;

    /**
     * The constructor for a Customers transaction
     *
     * @param amount      of money transferred
     * @param merchantId  receiving the money
     * @param customerId  sending the money
     * @param token       used to approve transaction
     * @param description description of the transaction
     * @param token       used to approve transaction
     */
    public Transaction(BigDecimal amount, String merchantId, String customerId, String token, String description) {
        this.amount = amount;
        this.merchantId = merchantId;
        this.customerId = customerId;
        this.token = token;
        this.description = description;
        timestamp = LocalDate.now();
    }

    /**
     * The constructor for a Merchants transaction
     *
     * @param amount of money transferred
     * @param token  used to approve transaction
     */
    public Transaction(BigDecimal amount, String token) {
        this.amount = amount;
        this.token = token;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public BigDecimal getAmount() {
        return amount;
    }

    @Override
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Override
    public String getMerchantId() {
        return merchantId;
    }

    @Override
    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    @Override
    public String getCustomerId() {
        return customerId;
    }

    @Override
    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    @Override
    public String getToken() {
        return token;
    }

    @Override
    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public LocalDate getTimestamp() {
        return timestamp;
    }

    @Override
    public void setTimestamp(LocalDate timestamp) {
        this.timestamp = timestamp;
    }
}
