package transactionservice.Entities;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * @author Sebastian Hoppe - s154306
 */
public interface ITransaction {
    String getId();
    void setId(String id);
    BigDecimal getAmount();
    void setAmount(BigDecimal amount);
    String getMerchantId();
    void setMerchantId(String merchant);
    String getCustomerId();
    void setCustomerId(String customer);
    String getToken();
    void setToken(String token);
    String getDescription();
    void setDescription(String description);
    LocalDate getTimestamp();
    void setTimestamp(LocalDate timestamp);

}
