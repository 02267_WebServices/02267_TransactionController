package transactionservice.Logic;

import java.math.BigDecimal;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import com.google.gson.Gson;

import transactionservice.Entities.Transaction;
import messaging.Event;
import messaging.EventReceiver;
import messaging.EventSender;

/**
 * @author Sebastian Hoppe - s154306
 */
public class TransactionManager implements EventReceiver {
    public static final String CREATE_TRANSACTION = "CreateTransaction";
    public static final String TRANSACTION_CREATED = "TransactionCreated";
    public static final String TRANSACTION_FAILED = "TransactionFailed";
    public static final String USER_TYPE_REQUEST = "UserTypeRequest";
    public static final String USER_TYPE_IDENTIFIED = "UserTypeIdentified";

    private EventSender sender;
    private TransactionController transactionController;
    private CompletableFuture<Event> future;

    /**
     * Constructor for the TransactionManager. This is the class that handles message queues
     *
     * @param sender                the event sender to send events to the message queue
     * @param transactionController the transaction controller handling all the logic
     */
    public TransactionManager(EventSender sender, TransactionController transactionController) {
        this.transactionController = transactionController;
        this.sender = sender;
    }

    /**
     * Used for creating a new transaction.
     *
     * @param event must be of type CREATE_TRANSACTION must contain args: int
     *              amount, String merchantId, String customerId, String token
     * @throws Exception <p>the event TRANSACTION_CREATED and has the new transaction as the argument </p>
     */
    public void createTransaction(Event event) throws Exception {
        if (!event.getEventType().equals(CREATE_TRANSACTION)) {
            return;
        }
        Object[] args = event.getArguments();
        BigDecimal amount = BigDecimal.valueOf((double) args[0]);
        String merchantId = args[1].toString();
        String customerId = args[2].toString();
        String token = args[3].toString();
        String description = args[4].toString();

        try {
            transactionController.createTransaction(amount, merchantId, customerId, token, description);

            Transaction transaction = transactionController.transactionDAO.getByToken(token);

            Event e = new Event(TRANSACTION_CREATED, new Object[]{new Gson().toJson(transaction)});
            sender.sendEvent(e);

        } catch (Exception e) {
            Event newEvent = new Event(TRANSACTION_FAILED, new Object[]{"failed"});
            sender.sendEvent(newEvent);
        }
    }

    /**
     * Requests usertype matching userId
     *
     * @param id of the user
     * @return user type
     * @throws Exception Exception
     */
    public Object[] getUserType(String id) throws Exception {
        try {
            Event e = new Event(USER_TYPE_REQUEST, new Object[]{id});
            future = new CompletableFuture<>();
            sender.sendEvent(e);

            future.get(1000, TimeUnit.MILLISECONDS);

            Event reportRequestResponse = future.join();
            boolean response = USER_TYPE_IDENTIFIED.equals(reportRequestResponse.getEventType());
            if (!response)
                throw new ClassNotFoundException();

            return reportRequestResponse.getArguments();

        } catch (TimeoutException e) {
            throw new ClassNotFoundException();
        }
    }

    @Override
    public void receiveEvent(Event event) throws Exception {
        if (event.getEventType().equals(USER_TYPE_IDENTIFIED)) {
            System.out.println("Event handled: " + event);
            future.complete(event);
        } else if (event.getEventType().equals(CREATE_TRANSACTION)) {
            createTransaction(event);
            //future.complete(event);
        }
    }
}
