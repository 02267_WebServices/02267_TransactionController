package transactionservice.Logic;

import transactionservice.Database.TransactionDAO;
import transactionservice.Entities.Transaction;
import com.google.gson.Gson;
import messaging.EventSender;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

/**
 * @author Sebastian Hoppe - s154306
 */
public class TransactionController {

    public TransactionDAO transactionDAO = new TransactionDAO();
    public TransactionManager tm;

    /**
     * The constructor for the TransactionController.
     *
     * @param eventSender Needs an EventSender to create the TransactionManager.
     */
    public TransactionController(EventSender eventSender) {
        this.tm = new TransactionManager(eventSender, this);
    }

    /**
     * Used to create a transaction in the transactionservice.Database.
     *
     * @param amount      transaction amount.
     * @param merchantId  id of the merchant
     * @param customerId  id of the customer
     * @param token       the Id of the token.
     * @param description description of the transaction.
     */
    public void createTransaction(BigDecimal amount, String merchantId, String customerId, String token, String description) {
        Transaction transaction = new Transaction(amount, merchantId, customerId, token, description);
        transactionDAO.addTransaction(transaction);
    }

    /**
     * Used for generating a report for a user.
     *
     * @param userId to generate a report for.
     * @param period only get the transactions created after this period.
     * @return a JSON object of the report.
     * @throws Exception Exception
     */
    public String getReport(String userId, LocalDate period) throws Exception {
        String userType = tm.getUserType(userId)[1].toString();
        return createReport(userId, period, userType);
    }

    private String createReport(String userId, LocalDate period, String userType) {
        List<Transaction> transactionsList;

        if (userType.equals("merchant")) {
            transactionsList = transactionDAO.getAllByMerchantIdFromPeriod(userId, period);
        } else if (userType.equals("manager")) {
            transactionsList = transactionDAO.getAll();
        } else {
            transactionsList = transactionDAO.getAllByCustomerIdFromPeriod(userId, period);
        }

        //Filter by period
        if (transactionsList.isEmpty()) {
            return "";
        }
        return new Gson().toJson(transactionsList);
    }
}
