package transactionservice.Logic;

import messaging.EventSender;
import messaging.rabbitmq.RabbitMqListener;
import messaging.rabbitmq.RabbitMqSender;

/**
 * @author Sebastian Hoppe - s154306
 */
public class TransactionControllerFactory {
    private static TransactionController transactionController = null;

    /**
     * Used to instantiate the transaction controller. This will also create a TransactionManager and thus enable message queues.
     *
     * @return TransactionController
     */
    public TransactionController getTransactionController() {
        if (transactionController != null) return transactionController;

        EventSender sender = new RabbitMqSender();
        TransactionController transactionController = new TransactionController(sender);
        TransactionManager tm = transactionController.tm;
        RabbitMqListener listener = new RabbitMqListener(tm);

        try {
            listener.listen();
        } catch (Exception e) {
            throw new Error(e);
        }

        return transactionController;
    }

}
