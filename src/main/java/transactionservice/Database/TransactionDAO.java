package transactionservice.Database;

import transactionservice.Entities.Transaction;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author Sebastian Hoppe - s154306
 */
public class TransactionDAO {
    Transaction trans1 = new Transaction(new BigDecimal(100), "1", "1", "1", "desc1");
    Transaction trans2 = new Transaction(new BigDecimal(150), "2", "2", "2", "desc2");

    static HashMap<String, Transaction> transactionDB;

    /**
     * Instansiates the dummyDB and fills it with two dummy entries.
     */
    public TransactionDAO() {
        transactionDB = new HashMap<>();
        transactionDB.put("1", trans1);
        transactionDB.put("2", trans2);
    }

    /**
     * Adds a new transaction to the database.
     *
     * @param transaction to be added to the DB.
     */
    public void addTransaction(Transaction transaction) {
        String key = "" + (transactionDB.size() + 1);
        transaction.setId(key);
        transactionDB.put(key, transaction);
    }

    /**
     * Return a transaction by given transaction id
     *
     * @param id of the transaction to return.
     * @return the transaction with given id.
     */
    public Transaction getById(String id) {
        return transactionDB.get(id);
    }

    /**
     * Used for getting all transactions for a report for customers.
     *
     * @param id     of the customer.
     * @param period latest date for getting transactinos.
     * @return a list of Transactions.
     */
    public List<Transaction> getAllByCustomerIdFromPeriod(String id, LocalDate period) {
        List<Transaction> result = new ArrayList<>();
        for (String key : transactionDB.keySet()) {
            Transaction transaction = transactionDB.get(key);
            if (transaction.getCustomerId().equals(id) && transaction.getTimestamp().isAfter(period)) {
                result.add(transaction);
            }
        }
        return result;
    }

    /**
     * Used for getting all transactions for a report for merchants.
     *
     * @param id     of the merchant.
     * @param period latest date for getting transactinos.
     * @return a list of Transactions.
     */
    public List<Transaction> getAllByMerchantIdFromPeriod(String id, LocalDate period) {
        List<Transaction> result = new ArrayList<>();
        for (String key : transactionDB.keySet()) {
            Transaction transaction = transactionDB.get(key);
            if (transaction.getMerchantId().equals(id) && transaction.getTimestamp().isAfter(period)) {
                transaction.setCustomerId("");
                result.add(transaction);
            }
        }
        return result;
    }

    /**
     * Get all transactions in the database.
     *
     * @return list of Transactions.
     */
    public List<Transaction> getAll() {
        List<Transaction> result = new ArrayList<>();
        for (String key : transactionDB.keySet()) {
            result.add(transactionDB.get(key));
        }
        return result;
    }

    /**
     * Gets a transaction with a given token id.
     *
     * @param token id of token.
     * @return transaction with given token id.
     */
    public Transaction getByToken(String token) {
        for (String key : transactionDB.keySet()) {
            Transaction transaction = transactionDB.get(key);
            if (transaction.getToken().equals(token)) {
                return transaction;
            }
        }
        return null;
    }
}
