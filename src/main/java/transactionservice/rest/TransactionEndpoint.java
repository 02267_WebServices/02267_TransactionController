package transactionservice.rest;

import java.time.LocalDate;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import transactionservice.Logic.TransactionController;
import transactionservice.Logic.TransactionControllerFactory;

/**
 * @author Sebastian Hoppe - s154306
 */
@Path("/transaction")
public class TransactionEndpoint {
  private static TransactionController transactionController = new TransactionControllerFactory()
      .getTransactionController();

  @GET
  @Path("/test")
  @Produces("text/plain")
  public Response doGet() {
    return Response.ok("Connected to Thorntail!").build();
  }

  @GET
  @Path("/report")
  @Produces("text/plain")
  public Response getReport(@QueryParam("cpr") String cpr, @QueryParam("period") String period) throws Exception {
    String result = null;
    try {
      result = transactionController.getReport(cpr, LocalDate.parse(period));
    } catch (ClassNotFoundException e) {
      return Response.status(Response.Status.NOT_FOUND).entity("404 Not Found").type(MediaType.TEXT_PLAIN).build();
    } catch (Exception e) {
      return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("500 Internal Server Error")
          .type(MediaType.TEXT_PLAIN).build();
    }
    return Response.ok(result).build();
  }
}
