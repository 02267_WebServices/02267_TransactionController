package messaging;

/**
 * @author Hubert Beumeister
 */
public interface EventSender {

	void sendEvent(Event event) throws Exception;

}
