package messaging;

/**
 * @author Hubert Baumeister
 */
public interface EventReceiver {
	void receiveEvent(Event event) throws Exception;
}
