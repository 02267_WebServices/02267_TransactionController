package messaging.rabbitmq;

import com.google.gson.Gson;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import messaging.Event;
import messaging.EventSender;

/**
 * @author Hubert Baumeister
 * Heavily inspired by the messageQueueDemo project
 */
public class RabbitMqSender implements EventSender {

    private static final String EXCHANGE_NAME = "eventsExchange";

    @Override
    public void sendEvent(Event event) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(System.getenv("RABBIT_MQ_HOSTNAME"));
        try (Connection connection = factory.newConnection(); Channel channel = connection.createChannel()) {
            channel.exchangeDeclare(EXCHANGE_NAME, "topic");
            String message = new Gson().toJson(event);
            AMQP.BasicProperties properties = new AMQP.BasicProperties.Builder()
                    .expiration("1000")
                    .build();
            System.out.println("[x] sending " + message);
            channel.basicPublish(EXCHANGE_NAME, "events", properties, message.getBytes("UTF-8"));
        }
    }

}
